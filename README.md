iw_master
=========

iw_master is a kickstaring master installation for TYPO3 CMS. This master is designed to be highly deployable.

The documentation will be available on a website, based on this master. This README.md should give you a first hint.


Installation
------------

Go to folder which is your htdocs directory and perform:

```
composer create-project instituteweb/iw_master:"dev-develop" ./ --ignore-platform-reqs --prefer-dist
```

When composer is ready, perform this to start the guided installer:

```
php iwm install
```

On Windows you can use
```
iwm.bat install
```

_Notice: Some setups require a different php binary (e.g. php_cli)_
