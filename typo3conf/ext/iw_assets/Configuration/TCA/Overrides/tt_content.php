<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

//$newColumns = array(
//	'tx_iwassets_example' => array(
//		'exclude' => 1,
//		'label' => 'LLL:EXT:iw_assets/Resources/Default/Private/Language/locallang_be.xlf:tca.tt_content.example',
//		'config' => array(
//			'type' => 'input',
//			'size' => 10,
//			'eval' => 'trim',
//		),
//	),
//);
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $newColumns);
