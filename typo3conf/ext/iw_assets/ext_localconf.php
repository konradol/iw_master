<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extensionKey]);
    if (isset($extensionConfiguration['addRootLineFields'])) {
        \InstituteWeb\Iwm\Utility\RootLineManager::addFields($extensionConfiguration['addRootLineFields']);
    }

    if (TYPO3_MODE === 'BE') {
        // Provide file based backend layouts
        if (!empty($extensionConfiguration['backendLayoutFileProviderDirectory'])) {
            $backendLayoutFileProviderDirectory = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(
                $extensionConfiguration['backendLayoutFileProviderDirectory']
            );

            $beFiles = \TYPO3\CMS\Core\Utility\GeneralUtility::getFilesInDir($backendLayoutFileProviderDirectory, 'ts');
            foreach ($beFiles as $beLayoutFileName) {
                $beLayoutPath = $backendLayoutFileProviderDirectory . DIRECTORY_SEPARATOR . $beLayoutFileName;
                \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(file_get_contents($beLayoutPath));
            }
        }
    }
};
$boot($_EXTKEY);
unset($boot);
