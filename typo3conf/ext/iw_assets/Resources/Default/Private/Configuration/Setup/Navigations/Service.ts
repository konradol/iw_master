# Navigation: Service Navigation
lib.navigations.service = HMENU
lib.navigations.service {
	special = directory
	special.value = {$global.pages.service}
	includeNotInMenu = 1

	1 = TMENU
	1 {
		NO = 1
		NO {
			wrapItemAndSub = <li>|</li>
			stdWrap.htmlSpecialChars = 1
		}
		ACT < .NO

		wrap = <ul>|</ul>
	}
}
