# TODO: Generate language configuration automatically with new sys_template service

# Setup for default language (eg. German)
config {
	language = de
	locale_all = de_DE.UTF8
	htmlTag_setParams = lang="de"

	sys_language_uid = 0

	# If a page is not translated handling
	sys_language_mode = content_fallback
}
page.bodyTagCObject.10.value = de


# Setup for foreign language (eg. English)
[globalVar = GP:L = 1]
config {
	language = en
	locale_all = en_GB.UTF8
	htmlTag_setParams = lang="en"

	sys_language_uid = 1
}
page.bodyTagCObject.10.value = en
[global]


# Display notice if current page is not translated
temp.currentProtocol = http://
[globalVar = IENV:TYPO3_SSL=1]
	temp.currentProtocol = https://
[global]

[userFunc = \InstituteWeb\Iwm\UserConditions\user_translationExistsNot()] && [globalVar = GP:L > 0]
	lib.contents.notTranslatedYet = COA
	lib.contents.notTranslatedYet {
		10 = TEXT
		10.data = {$translate}notTranslatedYet
		10.noTrimWrap = |<span>|</span> |

		20 = COA
		20 {
			10 = TEXT
			10.value < temp.currentProtocol

			20 = TEXT
			20.data = getIndpEnv:HTTP_HOST

			30 = TEXT
			30.typolink {
				parameter = {TSFE:id}
				parameter.insertData = 1
				returnLast = url
				addQueryString = 1
				addQueryString.exclude = L
				additionalParams = &L=0
			}

			wrap = <a href="|">
		}

		30 < .20
		30.wrap = |</a>

		wrap = <div class="notTranslatedYet">|</div>
	}

	lib.contents.columns.main.10 =< lib.contents.notTranslatedYet

		# Canonical Tag
	page.headerData.1391075690 < lib.contents.notTranslatedYet.20
	page.headerData.1391075690.wrap = <link rel="canonical" href="|">
[global]
