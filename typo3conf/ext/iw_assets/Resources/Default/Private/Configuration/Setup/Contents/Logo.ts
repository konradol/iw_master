lib.contents.logo = COA
lib.contents.logo {
    10 = IMAGE
    10 {
        file = EXT:iw_assets/Resources/Default/Public/Assets/Images/Logo.png
        file.width = 300
        altText.data = {$translate}companyName
        titleText.data = {$translate}companyName
        stdWrap.typolink.parameter = {$global.pages.home}
    }

    20 = TEXT
    20.data = {$translate}companySlogan
    20.wrap = <span class="slogan">|</span>
}
