plugin.tx_tinysource {
	enable = 1
	head {
		stripTabs = 1
		stripNewLines = 1
		stripDoubleSpaces = 1
		stripTwoLinesToOne = 1
	}
	body {
		stripComments = 1
		stripTabs = 1
		stripNewLines = 1
		stripDoubleSpaces = 1
		stripTwoLinesToOne = 1
		preventStripOfSearchComment = 1
	}
}
