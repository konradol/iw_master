# Assets includes (CSS/JS)

page.includeCSS {
	default = EXT:iw_assets/Resources/Default/Public/Assets/Stylesheets/default.css

#	ie = EXT:iw_assets/Resources/Default/Public/Assets/Stylesheets/ie.css
#	ie.allWrap = <!--[if lt IE 8]>|<![endif]-->
#	ie.excludeFromConcatenation = 1

#	print = EXT:iw_assets/Resources/Default/Public/Assets/Stylesheets/print.css
#	print.media = print
}

# JS libraries include
page.includeJSlibs {
	jquery = EXT:iw_assets/Resources/Default/Public/Libraries/Jquery/jquery-1.11.3.js
}

# JS includes
page.includeJSFooter {
	default = EXT:iw_assets/Resources/Default/Public/Assets/JavaScript/default.js
}
