# ----------------------------------------------------------------------------------------------- BEGIN: Local constants

local {
    languageLabels {
        current = Deutsch
        availableSysLanguageUids = 0,1
        availableLabels = Deutsch||English
    }
}

[globalVar = GP:L = 1]
    local.languageLabels {
        current = English
        availableLabels = Deutsch||Englisch
    }
[global]

# ------------------------------------------------------------------------------------------------- END: Local constants
# -------------------------------------------------------------------------------------------- BEGIN: Translate shortcut

# Usage in typoscript:
# 10 = TEXT
# 10.data = {$translate}key
translate = LLL:EXT:iw_assets/Resources/Default/Private/Language/locallang.xlf:

# ---------------------------------------------------------------------------------------------- END: Translate shortcut
