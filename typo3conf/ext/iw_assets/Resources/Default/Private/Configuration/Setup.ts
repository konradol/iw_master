# ------------------------------------------------------------------------------------------------ BEGIN:  Include setup

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/Config.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/Page.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/Templates.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/AssetIncludes.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/Languages.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/TtContent.ts">

<INCLUDE_TYPOSCRIPT: source="DIR:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/Extensions/" extensions="ts">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/Navigations/" extensions="ts">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/Contents/" extensions="ts">

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:iw_assets/Resources/Default/Private/Configuration/Setup/Debug.ts">

# --------------------------------------------------------------------------------------------------- END: Include setup
