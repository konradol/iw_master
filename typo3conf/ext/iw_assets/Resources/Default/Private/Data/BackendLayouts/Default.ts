mod.web_layout.BackendLayouts.Default {
	title = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Default.title
	config {
		backend_layout {
			colCount = 3
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Default.col.0
							colspan = 2
							colPos = 0
						}
						2 {
							name = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Default.col.2
							colPos = 2
						}
					}
				}
			}
		}
	}
}
