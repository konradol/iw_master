mod.web_layout.BackendLayouts.Special {
	title = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Special.title
	config {
		backend_layout {
			colCount = 4
			rowCount = 3
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Special.col.4
							colPos = 4
							colspan = 4
						}
					}
				}
				2 {
					columns {
						1 {
							name = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Special.col.1
							colPos = 1
						}
						2 {
							name = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Special.col.0
							colspan = 2
							colPos = 0
						}
						3 {
							name = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Special.col.2
							colPos = 2
						}
					}
				}
				3 {
					columns {
						1 {
							name = LLL:EXT:iw_assets/Resources/Default/Private/Language/BackendLayouts/locallang.xlf:Special.col.5
							colPos = 5
							colspan = 4
						}
					}
				}
			}
		}
	}
}
