tt_content.gridelements_pi1.20.10.setup.grid-33-33-33 {
	columns {
		20 {
			renderObj =< tt_content
			wrap = <div class="column left">|</div>
		}
		21 {
			renderObj =< tt_content
			wrap = <div class="column mid">|</div>
		}
		22 {
			renderObj =< tt_content
			wrap = <div class="column right">|</div>
		}
	}

	wrap = <div class="grid grid-33-33-33">|<div class="clear"></div></div>
	prepend < lib.stdheader
}
