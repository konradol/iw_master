<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is free  *
 *  | software and licensed under GNU General Public License.                 *
 *  |                                                                         *
 *  | (c) 2015-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>                      */

$EM_CONF[$_EXTKEY] = array(
    'title' => 'iw_assets',
    'description' => 'Project related assets and configurations.',
    'category' => 'plugin',
    'author' => 'Armin Ruediger Vieweg',
    'author_email' => 'vieweg@iwkoeln.de',
    'author_company' => 'Institut der deutschen Wirtschaft Köln Medien GmbH',
    'shy' => '',
    'dependencies' => '',
    'conflicts' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '7.0.0-dev',
    'constraints' => array(
        'depends' => array(
            'typo3' => '7.6.1-7.6.99',
            'iwm' => '7.0.0-7.99.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
    'suggests' => array(),
);
