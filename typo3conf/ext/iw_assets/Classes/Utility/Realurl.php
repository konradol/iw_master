<?php
namespace InstituteWeb\IwAssets\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */

/**
 * Class with userfunction to extend behaviour of realurl
 *
 * @package InstituteWeb\IwAssets
 */
class Realurl extends \InstituteWeb\Iwm\Realurl\AbstractRealurlUserFunctions
{

    /**
     * Replacements performing when creating links
     * from => to
     *
     * @var array
     */
    protected $replacementsInUrl = array(
        'news/detail/tx_news/' => 'news/show/',
    );

    /**
     * Like replacementsInUrl, but will be performed just in encode process
     *
     * @var array
     */
    protected $replacementsInUrlEncodeOnly = array(
        '/en.html' => '/en',
    );
}
