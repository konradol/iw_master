<?php

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
define('DS', DIRECTORY_SEPARATOR);

$environment = \InstituteWeb\Iwm\Environments\Environment::getInstance($GLOBALS['TYPO3_CONF_VARS']);
$environment->includeRootEnvironmentSettings();
$environment->callPhpConfigFile(__DIR__ . DS . 'Environments' . DS . 'All.php');
$environment->includeSubEnvironmentSettings();
$environment->apply();
