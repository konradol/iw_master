<?php
namespace InstituteWeb;

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\Environment;

/**
 * Here we define the base structure of our project, used in all environments.
 * We could do this in the configuration of the certain environments, but most likely the environments will
 * have the same base structure.
 *
 * Of course you can add additional content for certain environments e.g. a "playground-area" for dev or
 * different domain records. All new objects are registred in EntityManager automatically.
 *
 * To work with same objects over different files, you can use
 * $rootPage = $entityManager->get('root');
 *
 * This whole configuration works without uids. Instead you use identifiers, like "root". When importing to
 * database the actual uids are stored in the sys_registry of TYPO3.
 *
 * All this is just configuration, which can get debugged and executed on command line.
 *
 * @param EntityManager $entityManager
 * @param Environment $environment
 * @return callable function
 */
return function (EntityManager $entityManager, Environment $environment) {
    /*
     * File Storages
     */
    new Models\FileStorage('fileadmin/', 'storage.fileadmin', true);

    (new Models\FileStorage('Assets', 'storage.assets', false, 'typo3conf/ext/iw_assets/Resources/'))
        ->setIsWritable(false)
        ->setProcessingfolder('1:_processed_');

    /*
     * Languages 
     */
    $languages = [
        new Models\Language('German', 'lang.german', Models\Language::LANG_ENGLISH, Models\Language::FLAG_DE),
        new Models\Language('French', 'lang.french', Models\Language::LANG_FRENCH, Models\Language::FLAG_FR)
    ];

    /*
     * Root pages
     */
    $rootPageDefault = new Models\Pages\RootPage('Institute Web', 'pages.root.default', 512);
    $rootPageDefault
        ->addModifier(new Modifiers\Page\LimitTablesInListView(['tx_news_domain_model_news']))
        ->addModifier(new Modifiers\Page\RestrictAvailableLanguagesTo($languages))
        ->addModifier(new Modifiers\Page\SetDefaultLanguage('English', Models\Language::FLAG_EN_US_GB))

        ->setChildPages([
            new Models\Pages\Shortcut('Home', 'pages.home', 256, [], [], $rootPageDefault, Models\Pages\Shortcut::SHORTCUT_MODE_SELECTED_PAGE),

            new Models\Pages\SysFolder('Service Pages', 'pages.service', 512, [
                new Models\Pages\Page('Imprint', 'pages.service.imprint'),
                new Models\Pages\Page('Disclaimer', 'pages.service.disclaimer'),
                new Models\Pages\Page('Contact', 'pages.service.contact'),
                new Models\Pages\Page('404', 'pages.service.notfound', null, [], [
                    'nav_hide' => true,
                    'tx_realurl_pathsegment' => '404',
                    'tx_realurl_pathoverride' => '1'
                ]),
            ]),
            new Models\Pages\SysFolder('Local Storage', 'pages.localStorage', 768, [
                new Models\Pages\SysFolder('News', 'pages.localStorage.news'),
            ]),
        ]);


    /*
     * Root items
     */
    new Models\Pages\SysFolder('Global Storage', 'pages.storage', 1024, [
        new Models\Pages\SysFolder('Users', 'pages.storage.users'),
        new Models\Pages\SysFolder('Anything', 'pages.storage.anything'),
    ]);


    /*
     * Templates
     */
    $mainTemplate = new Models\Template('Main Template', 'template.main');
    $mainTemplate
        ->setIncludeStaticFile([
            'EXT:fluid_styled_content/Configuration/TypoScript/Static/',
            'EXT:fluid_styled_content/Configuration/TypoScript/Styling/',
            'EXT:gridelements/Configuration/TypoScript/',
            'EXT:news/Configuration/TypoScript',
            'EXT:powermail/Configuration/TypoScript/Main'
        ])
        ->addModifier(new Modifiers\Template\AddIdentifierConstantsMap())
        ->addModifier(new Modifiers\Template\IncludeConstantsTsFiles(['EXT:iw_assets/Resources/Default/Private/Configuration/Constants.ts']))
        ->addModifier(new Modifiers\Template\IncludeSetupTsFiles(['EXT:iw_assets/Resources/Default/Private/Configuration/Setup.ts']))
        
        ->addTo($rootPageDefault);
    
    /*
     * Define services, which do stuff for you in slot "EarlyOperations"
     */
    new Iwm\Environments\DataProvider\Services\ApplyDomainsInRealurl();
};
