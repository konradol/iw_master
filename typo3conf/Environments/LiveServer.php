<?php
namespace InstituteWeb;

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;

/**
 * @param EntityManager $entityManager
 * @param Environment $environment
 * @return callable function
 */
return function (EntityManager $entityManager, Environment $environment) {
    // Environment settings
    $environment->setDatabaseCredentials(
        'username',
        'password',
        'db',
        '127.0.0.1'
    );

    /** @var Models\Pages\RootPage $rootPageDefault */
    $rootPageDefault = $entityManager->get('root.default');
    $rootPageDefault->setDomains([
        new Models\Domain('www.domain.com', 'domain.primary', true),
        new Models\Domain('domain.com', 'domain.secondary', 'http://www.domain.com'),
        new Models\Domain('otherdomain.com', 'domain.secondary.short', 'http://www.domain.com')
    ]);
};
